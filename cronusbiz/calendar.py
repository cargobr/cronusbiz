from collections import defaultdict
import datetime

from . import easter


class Calendar:
    BRAZILIAN_FIX_HOLIDAYS = {
        1: [
            1,  # Confraternização Mundial
        ],
        4: [
            21,  # Tiradentes
        ],
        5: [
            1,  # Dia do Trabalhador
        ],
        6: [
            24,  # Dia de São Jão (Barueri)
        ],
        7: [
            9,  # Recolução Constitucionalista (São Paulo, estado)
        ],
        9: [
            7,  # Independência do Brasil
        ],
        10: [
            12,  # Nossa Senhora Aparecida
        ],
        11: [
            2,  # Finados
            15,  # Proclamação da República
            20,  # Consciência Negra (Barueri)
        ],
        12: [
            25,  # Natal
        ],
    }

    def __init__(self):
        self._fix_holidays = defaultdict(list, self.BRAZILIAN_FIX_HOLIDAYS)

    def _get_relative_holidays(self, year):
        easter_holidays = easter.relative_holidays(year)
        carnival_monday = easter_holidays['carnival_monday']
        carnival = easter_holidays['carnival']
        corpus_christi = easter_holidays['corpus_christi']
        holy_friday = easter_holidays['holy_friday']

        year_calendar = defaultdict(list)
        year_calendar[carnival_monday.month].append(carnival_monday.day)
        year_calendar[carnival.month].append(carnival.day)
        year_calendar[corpus_christi.month].append(corpus_christi.day)
        year_calendar[holy_friday.month].append(holy_friday.day)

        return year_calendar

    def is_holiday(self, when):
        try:
            year = when.year
            month = when.month
            day = when.day
        except AttributeError:
            raise ValueError('Give-me a date or datetime object.')

        relative = self._get_relative_holidays(year)
        fix = self._fix_holidays

        if not any([month in fix, month in relative]):
            return False

        if not any([day in fix[month], day in relative[month]]):
            return False

        return True

    def is_weekend(self, when):
        try:
            weekday = when.isoweekday()
        except AttributeError:
            raise ValueError('Give-me a date or datetime object.')

        return weekday > 5

    def is_businessday(self, when):
        return not any([self.is_holiday(when), self.is_weekend(when)])

    def grant_businessday(self, when):
        granted = when
        while not self.is_businessday(granted):
            granted += datetime.timedelta(days=1)

        return granted
