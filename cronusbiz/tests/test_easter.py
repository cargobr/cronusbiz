import unittest
import datetime

from .. import easter


class TestEasterClass(unittest.TestCase):
    def test_gauss_factor_valid_year(self):
        tests = [
            [1850, (24, 4)],
            [2018, (24, 5)],
            [2019, (24, 5)],
            [2200, (25, 7)],
            [2299, (25, 7)],
        ]

        for year, response in tests:
            with self.subTest(year=year):
                self.assertEqual(easter._gauss_factor(year), response)

    def test_gauss_factor_invalid_year(self):
        tests = [
            [1000, None],
            [5000, None],
        ]

        for year, response in tests:
            with self.subTest(year=year):
                self.assertRaises(Exception, easter._gauss_factor, year)

    def test_calc_easter(self):
        tests = [
            [2011, datetime.date(2011, 4, 24)],
            [2018, datetime.date(2018, 4, 1)],
            [2019, datetime.date(2019, 4, 21)],
            [2020, datetime.date(2020, 4, 12)],
            [2021, datetime.date(2021, 4, 4)],
            [2022, datetime.date(2022, 4, 17)],
            [2030, datetime.date(2030, 4, 21)],
        ]

        for year, easter_date in tests:
            with self.subTest(year=year):
                self.assertEqual(easter._calc_easter(year), easter_date)

    def test_relative_holidays(self):
        tests = {
            2011: {
                'easter': datetime.date(2011, 4, 24),
                'holy_friday': datetime.date(2011, 4, 22),
                'carnival_monday': datetime.date(2011, 3, 7),
                'carnival': datetime.date(2011, 3, 8),
                'corpus_christi': datetime.date(2011, 6, 23),
            },
            2018: {
                'easter': datetime.date(2018, 4, 1),
                'holy_friday': datetime.date(2018, 3, 30),
                'carnival_monday': datetime.date(2018, 2, 12),
                'carnival': datetime.date(2018, 2, 13),
                'corpus_christi': datetime.date(2018, 5, 31),
            },
            2019: {
                'easter': datetime.date(2019, 4, 21),
                'holy_friday': datetime.date(2019, 4, 19),
                'carnival_monday': datetime.date(2019, 3, 4),
                'carnival': datetime.date(2019, 3, 5),
                'corpus_christi': datetime.date(2019, 6, 20),
            },
            2020: {
                'easter': datetime.date(2020, 4, 12),
                'holy_friday': datetime.date(2020, 4, 10),
                'carnival_monday': datetime.date(2020, 2, 24),
                'carnival': datetime.date(2020, 2, 25),
                'corpus_christi': datetime.date(2020, 6, 11),
            },
            2021: {
                'easter': datetime.date(2021, 4, 4),
                'holy_friday': datetime.date(2021, 4, 2),
                'carnival_monday': datetime.date(2021, 2, 15),
                'carnival': datetime.date(2021, 2, 16),
                'corpus_christi': datetime.date(2021, 6, 3),
            },
            2022: {
                'easter': datetime.date(2022, 4, 17),
                'holy_friday': datetime.date(2022, 4, 15),
                'carnival_monday': datetime.date(2022, 2, 28),
                'carnival': datetime.date(2022, 3, 1),
                'corpus_christi': datetime.date(2022, 6, 16),
            },
            2030: {
                'easter': datetime.date(2030, 4, 21),
                'holy_friday': datetime.date(2030, 4, 19),
                'carnival_monday': datetime.date(2030, 3, 4),
                'carnival': datetime.date(2030, 3, 5),
                'corpus_christi': datetime.date(2030, 6, 20),
            },
        }

        for year, dates in tests.items():
            with self.subTest(year=year, dates=dates):
                self.assertEqual(easter.relative_holidays(year), dates)
