import setuptools


with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='cronusbiz',
    version='0.0.2',
    author='CARGOBR',
    author_email='ti@cargobr.com',
    description='A package to whether a day is a working day',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://bitbucket.com/cargobr/cronusbiz',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)
