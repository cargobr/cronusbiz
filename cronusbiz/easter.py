import datetime
from functools import lru_cache


def _gauss_factor(year):
    table = [
        (1800, 1899, 24, 4),
        (1900, 2099, 24, 5),
        (2100, 2199, 24, 6),
        (2200, 2299, 25, 7),
    ]

    for line in table:
        start, end, x, y = line

        if start <= year <= end:
            return x, y

    raise ValueError(
        'Invalid year: must be >= {} and <= {}'
        .format(table[0][0], table[-1][0])
    )


def _calc_easter(year):
    x, y = _gauss_factor(year)

    a = year % 19
    b = year % 4
    c = year % 7
    d = (19 * a + x) % 30
    e = (2 * b + 4 * c + 6 * d + y) % 7

    if d + e > 9:
        day = (d + e - 9)
        month = 4
    else:
        day = (d + e + 22)
        month = 3

    easter = datetime.date(year, month, day)

    return easter


@lru_cache()
def relative_holidays(year):
    easter = _calc_easter(year)
    holy_friday = easter - datetime.timedelta(2)
    carnival = easter - datetime.timedelta(47)
    carnival_monday = carnival - datetime.timedelta(1)
    corpus_christi = easter + datetime.timedelta(60)

    return {
        'easter': easter,
        'holy_friday': holy_friday,
        'carnival_monday': carnival_monday,
        'carnival': carnival,
        'corpus_christi': corpus_christi,
    }
