![https://cargobr.com](cargobr_full.png)
# CRONUSBIZ - A package to whether a day is a working day

Given a date, find out if it's a business day, holiday, or weekend. Also find
out, if it is not a business day, what next day is a business day.

## Testing

```
make test
```

## Usage

```
In [1]: from cronusbiz.calendar import Calendar

In [2]: import datetime

In [3]: calendar = Calendar()

In [4]: somedate = datetime.date(2018, 1, 1)

In [5]: print(calendar.is_holiday(somedate))
True

In [6]: print(calendar.is_businessday(somedate))
False

In [7]: print(calendar.is_weekend(somedate))
False

In [8]: print(calendar.grant_businessday(somedate))
2018-01-02
```