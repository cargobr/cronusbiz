import datetime
import unittest

from ..calendar import Calendar


class TestCalendar(unittest.TestCase):
    def test_have_fixed_holidays_in_init(self):
        calendar = Calendar()
        self.assertCountEqual(calendar._fix_holidays,
                              Calendar.BRAZILIAN_FIX_HOLIDAYS)

    def test__get_relative_holidays(self):
        expected = {
            2: [12, 13],
            3: [30, ],
            5: [31, ],
        }
        calendar = Calendar()
        self.assertEqual(calendar._get_relative_holidays(2018),
                              expected)

    def test_is_holiday(self):
        calendar = Calendar()
        tests = [
            [datetime.date(2001, 1, 1), True],
            [datetime.date(2001, 1, 2), False],
            [datetime.date(2019, 4, 21), True],
            [datetime.date(2020, 6, 11), True],
        ]

        for someday, expected in tests:
            with self.subTest(someday=someday, expected=expected):
                self.assertEqual(calendar.is_holiday(someday), expected)

    def test_is_weekend(self):
        calendar = Calendar()
        tests = [
            [datetime.date(2018, 1, 6), True],
            [datetime.date(2018, 3, 30), False],
            [datetime.date(2018, 4, 21), True],
            [datetime.date(2018, 12, 30), True],
        ]

        for someday, expected in tests:
            with self.subTest(someday=someday, expected=expected):
                self.assertEqual(calendar.is_weekend(someday), expected)

    def test_is_businessday(self):
        calendar = Calendar()
        tests = [
            [datetime.date(2018, 1, 9), True],
            [datetime.date(2018, 3, 30), False],
            [datetime.date(2018, 4, 21), False],
            [datetime.date(2018, 11, 20), False],
            [datetime.date(2018, 12, 31), True],
        ]

        for someday, expected in tests:
            with self.subTest(someday=someday, expected=expected):
                self.assertEqual(calendar.is_businessday(someday), expected)

    def test_grant_businessday(self):
        calendar = Calendar()
        tests = [
            [datetime.date(2018, 9, 1), datetime.date(2018, 9, 3)],
            [datetime.date(2018, 9, 7), datetime.date(2018, 9, 10)],
            [datetime.date(2018, 9, 14), datetime.date(2018, 9, 14)],
            [datetime.date(2018, 11, 2), datetime.date(2018, 11, 5)],
            [datetime.date(2018, 11, 14), datetime.date(2018, 11, 14)],
            [datetime.date(2018, 11, 15), datetime.date(2018, 11, 16)],
        ]

        for someday, expected in tests:
            with self.subTest(someday=someday, expected=expected):
                self.assertEqual(calendar.grant_businessday(someday), expected)
